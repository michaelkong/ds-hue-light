# README #

1. Start from console, cd into the project root folder, run npm install to get all node packages installed
2. To run the project in console

* To get a success signal, type in:
           node hue-light.js SUCCESS
* To get a failure signal, type in:
           node hue-light.js FAILED
* To get a maintenance signal, type in:
           node hue-light.js MAINTENANCE

3. To interact with Hue Api, we would need the bridge ip and a user account. Those two values can be find at configuration.js.

4. You can also change the colors by updating the color XY values at configuration.js
All colors can be find at http://www.developers.meethue.com/documentation/hue-xy-values

Hue colors can be set by using either via the xy value (xy : [0.2097, 0.6732]) or hue value (hue : 0). Since the xy value has higher precedence then hue value. If both xy value and hue value are set, the color associated with the xy value will used. 
