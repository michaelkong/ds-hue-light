var config = require('./.hue_lights'),
    request = require('request'),
    _ = require('underscore');

/**
 * Hue API functions
 */
function _makePutRequest(setup, light, settings, onSuccess, onFailure) {
  var apiSettings = settings;

  request({
    method: 'PUT',
    uri: 'http://' + setup.ip + '/api/' + setup.user + '/lights/' + light + '/state',
    body: JSON.stringify( apiSettings )
  }, function (error, response, body) {
      if (error) {
        if( onFailure ) {
          onFailure( error );
        }
      } else {
        if( onSuccess ) {
          onSuccess({responseBody: body});
        }
      }
  });
}


/**
 * Main
 */
var profileName = process.argv[2];
var ip = config.setup.ip;
var user = config.setup.user;
var profiles = config.profile;

var activeProfile = _(profiles).find(function(profile) {
    return profileName == profile.name;
  });

if( !activeProfile ) {
  console.log( "Sorry, we can't find the profile", profileName );
  process.exit(-1);
}

var bulbs = activeProfile.bulbs || [];
_(bulbs).each(function(bulb) {
  _makePutRequest( config.setup, bulb, activeProfile.settings, function() {
    console.log( "Successfully changed bulb", bulb );
  }, function( error ) {
    console.log( "Failed to change bulb", bulb );
    console.log( "Error encountered:", error );
  });
});
